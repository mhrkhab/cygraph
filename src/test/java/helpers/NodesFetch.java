package helpers;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import services.db.Neo4jDbConnection;

import java.util.ArrayList;

public class NodesFetch extends Neo4jDbConnection {

    private ArrayList<Long> ids;

    public NodesFetch(ArrayList<Long> ids){
        super();
        this.ids = ids;
    }

    public String getNodesName(){
        StringBuilder result = new StringBuilder();
        try(Transaction tx = this.graphDb.beginTx()){
            for (long id : ids) {
                Node node = tx.getNodeById(id);
                result.append(
                        node.getProperty("name").toString())
                        .append("(")
                        .append(node.getProperty("collection").toString())
                        .append("-")
                        .append(node.getProperty("type").toString())
                        .append(")");
            }
            tx.commit();
        }
        catch (Exception exception){
            return "";
        }
        shutDown();
        return result.toString();
    }
}
