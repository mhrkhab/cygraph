package models;

import models.missions.MissionInformation;
import org.bson.Document;
import org.bson.types.ObjectId;
import services.db.DBConnection;

import static com.mongodb.client.model.Filters.eq;

public class Machine extends Node{

    public final static String COLLECTION = "machines";

    public String ip;
    public String port;

    public Machine(
            String title,
            String description,
            String ip,
            String port
    ) {
        this.title = title;
        this.description = description;
        this.ip = ip;
        this.port = port;
    }

    public Machine(
            String title,
            String description,
            String ip,
            String port,
            ObjectId _id
    ) {
        this.title = title;
        this.description = description;
        this.ip = ip;
        this.port = port;
        this._id = _id;
    }


    @Override
    public void save() {
        DBConnection dbConnection = getConnection(COLLECTION);
        Document document = new Document("title",this.title)
                .append("description",this.description)
                .append("ip",this.ip)
                .append("port",this.port);
        saveOrUpdate(dbConnection,document);
    }

    public static Machine findByTitle(String title){
        return instance(
                (new DBConnection(COLLECTION)).find(
                        eq("title",title)
                )
        );
    }

    protected static Machine instance(Document document){
        return document != null ? new Machine(
                (String)document.get("title"),
                (String)document.get("description"),
                (String)document.get("ip"),
                (String)document.get("port"),
                (ObjectId)document.get("_id")
        ) : null;
    }

    public static Machine findByMainId(ObjectId _id) {
        Document document = findByMainIdAndDbCon(
                new DBConnection(COLLECTION),
                _id
        );

        if (document == null){
            return null;
        }
        return instance(document);
    }

    public String getCollection(){
        return COLLECTION;
    }
}
