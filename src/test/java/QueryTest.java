import helpers.NodesFetch;
import org.junit.jupiter.api.*;
import services.adapters.QuerySearcher;
import services.adapters.QuerySearcherResult;

public class QueryTest {

    @BeforeAll
    static void beforeAllTests() {
        System.out.println("test Queries and results");
        System.out.println("******************************************************");
    }

    @Test
    void querySample1Test() {
        showResult("MATCH p = (:ALERT {name:\"snort_33022\"})- [:SRC|DST|DETECTION|ON|ENABLES|AGAINST|PREPARES*]-> (:ALERT {name:\"snort_1576\"})  RETURN p");
    }

    @Test
    void querySample2Test() {
        showResult("MATCH p = ()-[*5]-() RETURN p");
    }

    @Test
    void querySample3Test() {
        showResult("MATCH p = (:MACHINE)-[:SRC]-> (:ALERT {name:\"snort_33022\"})-[:DETECTION]-> (:ATTACK_PATTERN)-[:AGAINST]-> (:VULNERABILITY)-[:ON]-> (:SUBNET) RETURN p");
    }

    void showResult(String query){
        QuerySearcherResult result = (new QuerySearcher()).search(query);
        System.out.println("Query ::");
        System.out.println(query);
        System.out.println("Result ::");
        System.out.println("number of rows = " + result.numberOfRows);
        System.out.println("number of columns = " + result.numberOfColumns);
        System.out.println("ids = " + result.ids.toString());
        System.out.println("indexed node id = " + result.indexNodeId);
        System.out.println("node names and info = " + new NodesFetch(result.ids).getNodesName());
        System.out.println("******************************************************");
    }
}
