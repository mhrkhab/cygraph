package models;

import org.bson.Document;
import org.bson.types.ObjectId;
import services.db.DBConnection;

import java.util.ArrayList;

import static com.mongodb.client.model.Filters.eq;

abstract public class Base {

    public ObjectId _id;
    public String title;
    public String description = null;


    protected static DBConnection getConnection(String collection){
        return new DBConnection(collection);
    }

    abstract public void save();

    abstract public String getCollection();

    public static Document findByMainIdAndDbCon(DBConnection dbConnection,Object _id){
        return dbConnection.find(eq("_id",_id));
    }

    public static Document findByIdAndDbCon(DBConnection dbConnection,String id){
        return dbConnection.find(eq("id",id));
    }

    public static ArrayList<Document> getAll(DBConnection dbConnection){
        return dbConnection.getAll();
    }

    public void saveOrUpdate(DBConnection dbConnection,Document document){
        if (this._id == null){
            dbConnection.save(document);
        }
        else {
            dbConnection.update(this._id,document);
        }
    }

}
