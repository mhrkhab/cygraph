package layouts;

import models.*;
import models.missions.*;
import models.vulnerabilities.*;
import services.adapters.GetNodeLabel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class QueryInfoLayout extends JFrame {

    public QueryInfoLayout(
            int numberOfRows,
            int numberOfColumns,
            int numberOfNodes,
            int numberOfRelations,
            int largestNodeDegree
    )
    {
        setTitle("Visualize Query Result");
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout());
        JTextArea textArea = new JTextArea(
                "\nQuery result has " + numberOfRows + " rows and "+ numberOfColumns + " columns :\n"
                + "\t" + numberOfNodes + " unique nodes\n"
                + "\t" + numberOfRelations + " unique relationships\n"
                +"Visualized Graph :\n"
                +"\t" + "Number of nodes : " + "  " + numberOfNodes +"\n"
                +"\t" + "Number of edges : " + "  " + numberOfRelations +"\n"
                +"\t" + "Average node degree : " + "  " + String.format("%.02f",((float)numberOfRelations/(numberOfNodes/2))) +"\n"
                +"\t" + "Largest node degree : " + "  " + largestNodeDegree
        );
        textArea.setEditable(false);
        textArea.setPreferredSize(new Dimension(430,150));
        cp.add(textArea);

        Label col_labels = new Label(".: Node Labels :.");
        cp.add(col_labels,BorderLayout.WEST);

        JPanel collectionsPanel = new JPanel();
        collectionsPanel.setLayout(new GridLayout(7,0));
        collectionsPanel.setPreferredSize(new Dimension(430,200));

        Label cpe_label = new Label(GetNodeLabel.getLabel(Vulnerability.COLLECTION)+" | "+ CPE.TYPE.toUpperCase());
        cpe_label.setForeground(new Color(93, 93, 138));
        collectionsPanel.add(cpe_label,BorderLayout.WEST);

        Label cvss_label = new Label(GetNodeLabel.getLabel(Vulnerability.COLLECTION)+" | "+ CVSS.TYPE.toUpperCase());
        cvss_label.setForeground(new Color(255, 0, 0));
        collectionsPanel.add(cvss_label,BorderLayout.WEST);

        Label metric_label = new Label(GetNodeLabel.getLabel(Vulnerability.COLLECTION)+" | "+ Metric.TYPE.toUpperCase());
        metric_label.setForeground(new Color(150, 60, 60));
        collectionsPanel.add(metric_label,BorderLayout.WEST);

        Label Severity_label = new Label(GetNodeLabel.getLabel(Vulnerability.COLLECTION)+" | "+ Severity.TYPE.toUpperCase());
        Severity_label.setForeground(new Color(169, 40, 40));
        collectionsPanel.add(Severity_label,BorderLayout.WEST);

        Label CVE_label = new Label(GetNodeLabel.getLabel(Vulnerability.COLLECTION)+" | "+ CVE.TYPE.toUpperCase());
        CVE_label.setForeground(new Color(7, 124, 2));
        collectionsPanel.add(CVE_label,BorderLayout.WEST);

        Label Reference_label = new Label(GetNodeLabel.getLabel(Vulnerability.COLLECTION)+" | "+ Reference.TYPE.toUpperCase());
        Reference_label.setForeground(new Color(12, 255, 0));
        collectionsPanel.add(Reference_label,BorderLayout.WEST);

        Label CWE_label = new Label(GetNodeLabel.getLabel(Vulnerability.COLLECTION)+" | "+ CWE.TYPE.toUpperCase());
        CWE_label.setForeground(new Color(255, 128, 0));
        collectionsPanel.add(CWE_label,BorderLayout.WEST);

        Label AttackPattern_label = new Label(GetNodeLabel.getLabel(AttackPattern.COLLECTION));
        AttackPattern_label.setForeground(new Color(57, 255, 255));
        collectionsPanel.add(AttackPattern_label,BorderLayout.WEST);

        Label Machine_label = new Label(GetNodeLabel.getLabel(Machine.COLLECTION));
        Machine_label.setForeground(new Color(170, 0, 255));
        collectionsPanel.add(Machine_label,BorderLayout.WEST);

        Label Firewall_label = new Label(GetNodeLabel.getLabel(Firewall.COLLECTION));
        Firewall_label.setForeground(new Color(36, 150, 132));
        collectionsPanel.add(Firewall_label,BorderLayout.WEST);

        Label Subnet_label = new Label(GetNodeLabel.getLabel(Subnet.COLLECTION));
        Subnet_label.setForeground(new Color(255, 233, 0));
        collectionsPanel.add(Subnet_label,BorderLayout.WEST);

        Label Snort_label = new Label(GetNodeLabel.getLabel(Snort.COLLECTION));
        Snort_label.setForeground(new Color(121, 0, 0));
        collectionsPanel.add(Snort_label,BorderLayout.WEST);

        Label MissionTask_label = new Label(GetNodeLabel.getLabel(MissionTask.COLLECTION));
        MissionTask_label.setForeground(new Color(2, 114, 154));
        collectionsPanel.add(MissionTask_label,BorderLayout.WEST);

        Label MissionInformation_label = new Label(GetNodeLabel.getLabel(MissionInformation.COLLECTION));
        MissionInformation_label.setForeground(new Color(164, 124, 189));
        collectionsPanel.add(MissionInformation_label,BorderLayout.WEST);

        Label CyberAsset_label = new Label(GetNodeLabel.getLabel(CyberAsset.COLLECTION));
        CyberAsset_label.setForeground(new Color(54, 12, 87, 65));
        collectionsPanel.add(CyberAsset_label,BorderLayout.WEST);

        Label MissionObj_label = new Label(GetNodeLabel.getLabel(MissionObjective.COLLECTION));
        MissionObj_label.setForeground(new Color(54, 12, 87, 245));
        collectionsPanel.add(MissionObj_label,BorderLayout.WEST);

        cp.add(collectionsPanel,BorderLayout.WEST);

        Button cancel = new Button("Cancel");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        cp.add(cancel,BorderLayout.CENTER);

        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setVisible(true);
        setSize(445,450);
        setLocation(1000,276);
    }
}
