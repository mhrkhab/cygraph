package services.adapters;

import layouts.MistakeQuery;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import services.db.Neo4jDbConnection;

import java.util.ArrayList;
import java.util.Map;

public class QuerySearcher extends Neo4jDbConnection {

    private int numberOfRows = 0;
    private int numberOfColumns = 0;
    private final ArrayList<Long> ids = new ArrayList<>();
    private long indexNodeId = -1;

    public QuerySearcherResult search(String query){
        query = new StringBuffer(query).replace(
                query.lastIndexOf("RETURN"),
                query.length(),
                "RETURN [node in nodes(p) | id(node)]"
        ).toString();

        try(Transaction tx = this.graphDb.beginTx();
            Result result = tx.execute(query)
        ){
            while ( result.hasNext() )
            {
                Map<String,Object> row = result.next();
                numberOfRows += row.size();
                for ( Map.Entry<String,Object> column : row.entrySet() )
                {
                    if (column.getValue() instanceof ArrayList){
                        if (((ArrayList) column.getValue()).size() > numberOfColumns){
                            numberOfColumns = ((ArrayList) column.getValue()).size();
                        }
                        ArrayList<Long> nodes_helper = new ArrayList<>();
                        for (Object id : (ArrayList)column.getValue()) {
                            if (indexNodeId == -1){
                                indexNodeId = (long)id;
                            }
                            nodes_helper.add((long)id);
                        }
                        if (nodes_helper.contains(indexNodeId)){
                            for (long id : nodes_helper) {
                                if (!ids.contains(id)){
                                    ids.add(id);
                                }
                            }
                        }
                    }
                }
            }
            tx.commit();
        }
        catch (Exception exception){
            new MistakeQuery();
        }
        shutDown();

        return new QuerySearcherResult(numberOfRows, numberOfColumns, ids, indexNodeId);
    }
}
