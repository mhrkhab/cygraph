package services.db;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import org.bson.types.ObjectId;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.dbms.api.DatabaseManagementServiceBuilder;
import org.neo4j.graphdb.*;
import services.adapters.GetNodeLabel;

import static org.neo4j.configuration.GraphDatabaseSettings.DEFAULT_DATABASE_NAME;

public class Neo4jDbConnection {
    private static final Path databaseDirectory = Path.of( "target/neo4j-CyGraph-db" );

    // tag::vars[]
    protected GraphDatabaseService graphDb;
    protected DatabaseManagementService managementService;
    // end::vars[]

    // tag::createReltype[]
    private enum RelTypes implements RelationshipType
    {
        KNOWS
    }
    // end::createReltype[]

    public Neo4jDbConnection(){
        // tag::startDb[]
        this.managementService = new DatabaseManagementServiceBuilder( databaseDirectory ).build();
        this.graphDb = this.managementService.database( DEFAULT_DATABASE_NAME );
        registerShutdownHook( this.managementService );
        // end::startDb[]
    }

    public Node storeNode(HashMap<String,String> properties) throws IOException{
        Node node = null;
        // tag::transaction[]
        try ( Transaction tx = this.graphDb.beginTx() ) {
            // end::transaction[]
            // tag::addData[]
            node = tx.createNode();

            for (Map.Entry<String, String> property : properties.entrySet()) {
                node.setProperty(property.getKey(),property.getValue());
            }
            // tag::transaction[]
            tx.commit();
            shutDown();
        }
        return node;
    }

    public Relationship relating(Node node1,Node node2,HashMap<String,String> properties) throws IOException{
        Relationship relationship;
        // tag::transaction[]
        try ( Transaction tx = this.graphDb.beginTx() )
        {
            node1 = tx.getNodeById(node1.getId());
            node2 = tx.getNodeById(node2.getId());
            relationship = node1.createRelationshipTo(node2,RelTypes.KNOWS);
            for (Map.Entry<String, String> property : properties.entrySet()) {
                relationship.setProperty(property.getKey(),property.getValue());
            }
            tx.commit();
            shutDown();
        }
        return relationship;
    }

    // tag::shutdownHook[]
    private static void registerShutdownHook( final DatabaseManagementService managementService )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                managementService.shutdown();
            }
        });
    }
    // end::shutdownHook[]

    public void removeNodeAndRelations(Node node)
    {
        try ( Transaction tx = this.graphDb.beginTx() )
        {
            // tag::removingData[]
            // let's remove the data
            node = tx.getNodeById( node.getId() );
            for (Relationship rel : node.getRelationships(RelTypes.KNOWS)) {
                rel.delete();
            }
            node.delete();
            // end::removingData[]
            tx.commit();
            shutDown();
        }
    }

    protected void shutDown()
    {
        // tag::shutdownServer[]
        managementService.shutdown();
        // end::shutdownServer[]
    }

    public long getNodeIdBySearchOnIdentifier(String identifier){
        long res = -1;
        try ( Transaction tx = this.graphDb.beginTx();
              Result result = tx.execute( "MATCH (n {identifier: '"+identifier+"'}) RETURN ID(n)" ))
        {
            while ( result.hasNext() )
            {
                Map<String,Object> row = result.next();
                for ( Map.Entry<String,Object> column : row.entrySet() )
                {
                    res = (long) column.getValue();
                }
            }
            tx.commit();
            shutDown();
        }
        return res;
    }

    public Node findNodeById(long id){
        Node node;
        try ( Transaction tx = this.graphDb.beginTx() )
        {
            node =  tx.getNodeById(id);
            tx.commit();
            shutDown();
        }
        return node;
    }

    public Map<String, Object> getNodeProperties(Node node){
        Map<String, Object> props;
        try ( Transaction tx = this.graphDb.beginTx() )
        {
            props = tx.getNodeById(node.getId()).getAllProperties();
            tx.commit();
            shutDown();
        }
        return props;
    }

    public ArrayList<Long> getNodeRelationships(Node node){
        Iterator<Relationship> relationships;
        ArrayList<Long> result = new ArrayList<>();
        try ( Transaction tx = this.graphDb.beginTx() )
        {
            relationships =  tx.getNodeById(node.getId()).getRelationships().iterator();
            while (relationships.hasNext()){
                result.add(relationships.next().getId());
            }
            tx.commit();
            shutDown();
        }
        return result;
    }

    public Node getRelationshipEndNode(long relationshipId){
        Node endNode;
        try ( Transaction tx = this.graphDb.beginTx() )
        {
            endNode =  tx.getRelationshipById(relationshipId).getEndNode();
            tx.commit();
            shutDown();
        }
        return endNode;
    }

    public Map<String, Object> getRelationshipProperties(long relationshipId){
        Map<String, Object> props;

        try ( Transaction tx = this.graphDb.beginTx() )
        {
            props = tx.getRelationshipById(relationshipId).getAllProperties();
            tx.commit();
            shutDown();
        }
        return props;
    }

    protected Node storeOrGetNode(ObjectId id, String name, String collection,Transaction tx, String type) {
        long originNodeId = -1;
        Result result = tx.execute( "MATCH (n {identifier: '"+id.toString()+"'}) RETURN ID(n)" );
        while ( result.hasNext() )
        {
            Map<String,Object> row = result.next();
            for ( Map.Entry<String,Object> column : row.entrySet() )
            {
                originNodeId = (long) column.getValue();
            }
        }

        if (originNodeId == -1){
            type = type == null ? "" : type;
            HashMap<String,String> properties = new HashMap<>();
            properties.put("identifier",id.toString());
            properties.put("name",name);
            properties.put("collection",collection);
            properties.put("type",type);
            Node node;
            if (!type.isEmpty() && !type.isBlank()){
                node = tx.createNode(
                        Label.label(GetNodeLabel.getLabel(collection)),
                        Label.label(type.toUpperCase())
                );
            }
            else {
                node = tx.createNode(
                        Label.label(GetNodeLabel.getLabel(collection))
                );
            }
            for (Map.Entry<String, String> property : properties.entrySet()) {
                node.setProperty(property.getKey(),property.getValue());
            }
            return node;
        }
        else {
            return tx.getNodeById(originNodeId);
        }
    }

    protected static Relationship relationship(String name,Node node1,Node node2,Transaction tx) throws IOException {
        HashMap<String,String> properties = new HashMap<>();
        properties.put("name",name);

        node1 = tx.getNodeById(node1.getId());
        node2 = tx.getNodeById(node2.getId());
        Relationship relationship = node1.createRelationshipTo(node2,RelationshipType.withName(name.toUpperCase()));
        for (Map.Entry<String, String> property : properties.entrySet()) {
            relationship.setProperty(property.getKey(),property.getValue());
        }
        return relationship;
    }

    protected Node getNodeById(long id,Transaction tx){
        return  tx.getNodeById(id);
    }
}
