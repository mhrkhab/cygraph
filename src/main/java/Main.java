import layouts.Launch;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        new Launch();
    }
}