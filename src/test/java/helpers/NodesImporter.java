package helpers;

import models.*;
import models.missions.MissionInformation;
import models.missions.MissionTask;
import models.vulnerabilities.*;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import services.db.Neo4jDbConnection;

public class NodesImporter extends Neo4jDbConnection {

    public void run(){
        try(Transaction tx = this.graphDb.beginTx()){
            AttackPattern at1 = AttackPattern.findById("100");
            Node at1node = storeOrGetNode(at1._id, at1.title, at1.getCollection(), tx, null);

            nodeInfoPrint(at1node);

            AttackPattern at2 = AttackPattern.findById("224");
            Node at2node = storeOrGetNode(at2._id, at2.title, at2.getCollection(), tx, null);

            nodeInfoPrint(at2node);

            Machine machine = Machine.findByTitle("LocalDnsServer");
            Node ma1 = storeOrGetNode(machine._id, machine.title, machine.getCollection(), tx, null);

            nodeInfoPrint(ma1);

            relationship("VICTIM", at2node, ma1, tx);

            AttackPattern attackPattern142 = AttackPattern.findById("142");
            Node attackPattern142Node = storeOrGetNode(attackPattern142._id, attackPattern142.title, attackPattern142.getCollection(), tx, null);

            nodeInfoPrint(attackPattern142Node);

            AttackPattern attackPattern89 = AttackPattern.findById("89");
            Node attackPattern89Node = storeOrGetNode(attackPattern89._id, attackPattern89.title, attackPattern89.getCollection(), tx, null);

            nodeInfoPrint(attackPattern89Node);

            AttackPattern attackPattern14 = AttackPattern.findById("14");
            Node attackPattern14Node = storeOrGetNode(attackPattern14._id, attackPattern14.title, attackPattern14.getCollection(), tx, null);

            nodeInfoPrint(attackPattern14Node);

            AttackPattern attackPattern205 = AttackPattern.findById("205");
            Node attackPattern205Node = storeOrGetNode(attackPattern205._id, attackPattern205.title, attackPattern205.getCollection(), tx, null);

            nodeInfoPrint(attackPattern205Node);

            AttackPattern attackPattern600 = AttackPattern.findById("600");
            Node attackPattern600Node = storeOrGetNode(attackPattern600._id, attackPattern600.title, attackPattern600.getCollection(), tx, null);

            nodeInfoPrint(attackPattern600Node);

            AttackPattern attackPattern170 = AttackPattern.findById("170");
            Node attackPattern170Node = storeOrGetNode(attackPattern170._id, attackPattern170.title, attackPattern170.getCollection(), tx, null);

            nodeInfoPrint(attackPattern170Node);

            AttackPattern attackPattern110 = AttackPattern.findById("110");
            Node attackPattern110Node = storeOrGetNode(attackPattern110._id, attackPattern110.title, attackPattern110.getCollection(), tx, null);

            nodeInfoPrint(attackPattern110Node);

            relationship("PREPARES", attackPattern142Node, attackPattern89Node, tx);
            relationship("PREPARES", attackPattern89Node, attackPattern14Node, tx);
            relationship("PREPARES", attackPattern14Node, attackPattern205Node, tx);
            relationship("PREPARES", attackPattern205Node, attackPattern600Node, tx);
            relationship("PREPARES", attackPattern600Node, attackPattern170Node, tx);
            relationship("PREPARES", attackPattern170Node, attackPattern110Node, tx);

            Machine attackerMachine = Machine.findByTitle("Attacker");
            Node attackerMachineNode = storeOrGetNode(attackerMachine._id, attackerMachine.title, attackerMachine.getCollection(), tx, null);

            nodeInfoPrint(attackerMachineNode);

            Machine localDnsServerMachine = Machine.findByTitle("LocalDnsServer");
            Node localDnsServerMachineNode = storeOrGetNode(localDnsServerMachine._id, localDnsServerMachine.title, localDnsServerMachine.getCollection(), tx, null);

            nodeInfoPrint(localDnsServerMachineNode);

            Machine maliolousMachine = Machine.findByTitle("malicous.com");
            Node maliolousMachineNode = storeOrGetNode(maliolousMachine._id, maliolousMachine.title, maliolousMachine.getCollection(), tx, null);

            nodeInfoPrint(maliolousMachineNode);

            Machine missionClientMachine = Machine.findByTitle("MissionClient");
            Node missionClientMachineNode = storeOrGetNode(missionClientMachine._id, missionClientMachine.title, missionClientMachine.getCollection(), tx, null);

            nodeInfoPrint(missionClientMachineNode);

            Machine DatabaseFrontendMachine = Machine.findByTitle("DatabaseFrontend");
            Node DatabaseFrontendMachineNode = storeOrGetNode(DatabaseFrontendMachine._id, DatabaseFrontendMachine.title, DatabaseFrontendMachine.getCollection(), tx, null);

            nodeInfoPrint(DatabaseFrontendMachineNode);

            Machine DatabaseBackendMachine = Machine.findByTitle("DatabaseBackend");
            Node DatabaseBackendMachineNode = storeOrGetNode(DatabaseBackendMachine._id, DatabaseBackendMachine.title, DatabaseBackendMachine.getCollection(), tx, null);

            nodeInfoPrint(DatabaseBackendMachineNode);

            relationship("LAUNCHES", attackerMachineNode, attackPattern142Node, tx);
            relationship("VICTIM", attackPattern142Node, localDnsServerMachineNode, tx);
            relationship("LAUNCHES", maliolousMachineNode, attackPattern89Node, tx);
            relationship("LAUNCHES", maliolousMachineNode, attackPattern14Node, tx);
            relationship("VICTIM", attackPattern89Node, missionClientMachineNode, tx);
            relationship("VICTIM", attackPattern14Node, missionClientMachineNode, tx);
            relationship("LAUNCHES", attackPattern205Node, missionClientMachineNode, tx);
            relationship("LAUNCHES", missionClientMachineNode, attackPattern205Node, tx);
            relationship("LAUNCHES", missionClientMachineNode, attackPattern600Node, tx);
            relationship("LAUNCHES", missionClientMachineNode, attackPattern170Node, tx);
            relationship("LAUNCHES", missionClientMachineNode, attackPattern110Node, tx);
            relationship("VICTIM", attackPattern600Node, DatabaseFrontendMachineNode, tx);
            relationship("VICTIM", attackPattern170Node, DatabaseFrontendMachineNode, tx);
            relationship("VICTIM", attackPattern110Node, DatabaseBackendMachineNode, tx);

            Subnet InternetSubnet = Subnet.findByTitle("Internet");
            Node InternetSubnetNode = storeOrGetNode(InternetSubnet._id, InternetSubnet.title, InternetSubnet.getCollection(), tx, null);

            nodeInfoPrint(InternetSubnetNode);

            Subnet DMZSubnet = Subnet.findByTitle("DMZ");
            Node DMZSubnetNode = storeOrGetNode(DMZSubnet._id, DMZSubnet.title, DMZSubnet.getCollection(), tx, null);

            nodeInfoPrint(DMZSubnetNode);

            Subnet MissionClientsSubnet = Subnet.findByTitle("MissionClients");
            Node MissionClientsSubnetNode = storeOrGetNode(MissionClientsSubnet._id, MissionClientsSubnet.title, MissionClientsSubnet.getCollection(), tx, null);

            nodeInfoPrint(MissionClientsSubnetNode);

            Subnet DataCenterSubnet = Subnet.findByTitle("DataCenter");
            Node DataCenterSubnetNode = storeOrGetNode(DataCenterSubnet._id, DataCenterSubnet.title, DataCenterSubnet.getCollection(), tx, null);

            nodeInfoPrint(DataCenterSubnetNode);

            relationship("INN", InternetSubnetNode, attackerMachineNode, tx);
            relationship("INN", attackerMachineNode, InternetSubnetNode, tx);
            relationship("INN", maliolousMachineNode, InternetSubnetNode, tx);
            relationship("INN", InternetSubnetNode, maliolousMachineNode, tx);
            relationship("INN", localDnsServerMachineNode, DMZSubnetNode, tx);
            relationship("INN", missionClientMachineNode, MissionClientsSubnetNode, tx);
            relationship("INN", MissionClientsSubnetNode, missionClientMachineNode, tx);
            relationship("INN", DataCenterSubnetNode, DatabaseFrontendMachineNode, tx);
            relationship("INN", DatabaseFrontendMachineNode, DataCenterSubnetNode, tx);
            relationship("INN", DatabaseBackendMachineNode, DataCenterSubnetNode, tx);

            Firewall externalFirewall = Firewall.findByTitle("externalFirewall");
            Node externalFirewallNode = storeOrGetNode(externalFirewall._id, externalFirewall.title, externalFirewall.getCollection(), tx, null);

            nodeInfoPrint(externalFirewallNode);

            Firewall internalFirewall = Firewall.findByTitle("internalFirewall");
            Node internalFirewallNode = storeOrGetNode(internalFirewall._id, internalFirewall.title, internalFirewall.getCollection(), tx, null);

            nodeInfoPrint(internalFirewallNode);

            relationship("ROUTES", externalFirewallNode, InternetSubnetNode, tx);
            relationship("ROUTES", externalFirewallNode, DMZSubnetNode, tx);
            relationship("ROUTES", externalFirewallNode, MissionClientsSubnetNode, tx);
            relationship("ROUTES", internalFirewallNode, MissionClientsSubnetNode, tx);
            relationship("ROUTES", internalFirewallNode, DataCenterSubnetNode, tx);

            CVE cve20041754 = CVE.findById("CVE-2004-1754");
            Node cve20041754Node = storeOrGetNode(cve20041754._id, cve20041754.title, cve20041754.getCollection(), tx, cve20041754.type);

            nodeInfoPrint(cve20041754Node);

            CVE cve202141322 = CVE.findById("CVE-2021-41322");
            Node cve202141322Node = storeOrGetNode(cve202141322._id, cve202141322.title, cve202141322.getCollection(), tx, cve202141322.type);

            nodeInfoPrint(cve202141322Node);

            relationship("AGAINST", attackPattern142Node, cve20041754Node, tx);
            relationship("ON", cve20041754Node, localDnsServerMachineNode, tx);
            relationship("AGAINST", attackPattern14Node, cve202141322Node, tx);
            relationship("ON", cve202141322Node, MissionClientsSubnetNode, tx);

            Snort snort33022 = Snort.findById("33022");
            Node snort33022Node = storeOrGetNode(snort33022._id, snort33022.title, snort33022.getCollection(), tx, null);

            nodeInfoPrint(snort33022Node);

            relationship("SRC", maliolousMachineNode, snort33022Node, tx);
            relationship("DETECTION", snort33022Node, attackPattern14Node, tx);
            relationship("DETECTION", attackPattern14Node, snort33022Node, tx);
            relationship("DST", snort33022Node, DatabaseBackendMachineNode, tx);

            Snort snort1576 = Snort.findById("1576");
            Node snort1576Node = storeOrGetNode(snort1576._id, snort1576.title, snort1576.getCollection(), tx, null);

            nodeInfoPrint(snort1576Node);

            relationship("SRC", DatabaseBackendMachineNode, snort1576Node, tx);
            relationship("DETECTION", attackPattern170Node, snort1576Node, tx);
            relationship("DETECTION", snort1576Node, attackPattern170Node, tx);
            relationship("ENABLES", DatabaseBackendMachineNode, attackPattern14Node, tx);

            MissionInformation missionInformation1 = MissionInformation.findByTitle("mission_information_1");
            Node missionInformation1Node = storeOrGetNode(missionInformation1._id, missionInformation1.title, missionInformation1.getCollection(), tx, null);

            nodeInfoPrint(missionInformation1Node);

            MissionInformation missionInformation2 = MissionInformation.findByTitle("mission_information_2");
            Node missionInformation2Node = storeOrGetNode(missionInformation2._id, missionInformation2.title, missionInformation2.getCollection(), tx, null);

            nodeInfoPrint(missionInformation2Node);

            MissionTask missionTask1 = MissionTask.findByTitle("mission_task_1");
            Node missionTask1Node = storeOrGetNode(missionTask1._id, missionTask1.title, missionTask1.getCollection(), tx, null);

            nodeInfoPrint(missionTask1Node);

            MissionTask missionTask2 = MissionTask.findByTitle("mission_task_2");
            Node missionTask2Node = storeOrGetNode(missionTask2._id, missionTask2.title, missionTask2.getCollection(), tx, null);

            nodeInfoPrint(missionTask2Node);

            relationship("HIGH", missionTask1Node, missionInformation1Node, tx);
            relationship("LOW", missionTask1Node, missionInformation2Node, tx);
            relationship("HIGH", missionTask2Node, missionInformation2Node, tx);
            relationship("LOW", missionInformation1Node, localDnsServerMachineNode, tx);
            relationship("HIGH", missionInformation2Node, attackerMachineNode, tx);

            CPE cpe1 = CPE.findCveCpeByCpe23UriAndCveId("cpe:2.3:a:sap:netweaver_abap:730:*:*:*:*:*:*:*", cve20041754._id);
            Node cpe1Node = storeOrGetNode(cpe1._id, cpe1.cpe23Uri, cpe1.getCollection(), tx, cpe1.type);

            nodeInfoPrint(cpe1Node);

            relationship("SOFTWARE", cve20041754Node, cpe1Node, tx);

            CWE cwe1 = CWE.findCveCweByCweIdAndCveId("CWE-668", cve20041754._id);
            Node cwe1Node = storeOrGetNode(cwe1._id, cwe1.id, cwe1.getCollection(), tx, cwe1.type);

            nodeInfoPrint(cwe1Node);

            relationship("CWE", cve20041754Node, cwe1Node, tx);

            CVSS cvss1 = CVSS.findCveCvssByTitleAndCveId("cvssV3", cve20041754._id);
            Node cvss1Node = storeOrGetNode(cvss1._id, cvss1.title, cvss1.getCollection(), tx, cvss1.type);

            nodeInfoPrint(cvss1Node);

            relationship("CVSS", cve20041754Node, cvss1Node, tx);

            Metric metric1 = Metric.findCvssMetricByTitleAndCveId("baseMetricV3", cve20041754._id);
            Node metric1Node = storeOrGetNode(metric1._id, metric1.title, metric1.getCollection(), tx, metric1.type);

            nodeInfoPrint(metric1Node);

            relationship("METRIC", cvss1Node, metric1Node, tx);

            Severity severity1 = Severity.findByTitleAndCveId("cvssV2", cve20041754._id);
            Node severity1Node = storeOrGetNode(severity1._id, severity1.title, severity1.getCollection(), tx, severity1.type);

            nodeInfoPrint(severity1Node);

            relationship("SEVERITY", cvss1Node, severity1Node, tx);

            tx.commit();
        }
        catch (Exception exception){
            exception.printStackTrace();
        }
        shutDown();
    }

    private void nodeInfoPrint(Node node){
        System.out.println(
                "Node :: id=" + node.getId() +
                        " - name = " + node.getProperty("name") +
                        " - collection = " + node.getProperty("collection") +
                        " - type = " + node.getProperty("type")
                );
    }
}
