package services.adapters;

import models.Query;

public class QueryBuilder{

    public QueryBuilder(String query) {
        super();
        if (query.isEmpty() || query.isBlank()) {
            return;
        }

        if (Query.findByQuery(query) == null) {
            Query queryRecord = new Query(query);
            queryRecord.save();
        }

        QuerySearcherResult result = (new QuerySearcher()).search(query);

        (new GraphPlotter()).plot(result);
    }
}
