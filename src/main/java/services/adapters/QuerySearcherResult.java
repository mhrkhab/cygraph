package services.adapters;

import java.util.ArrayList;

public class QuerySearcherResult {
    public int numberOfRows;
    public int numberOfColumns;
    public ArrayList<Long> ids;
    public long indexNodeId;

    public QuerySearcherResult(int numberOfRows, int numberOfColumns, ArrayList<Long> ids,long indexNodeId){
        this.ids = ids;
        this.numberOfColumns = numberOfColumns;
        this.numberOfRows = numberOfRows;
        this.indexNodeId = indexNodeId;
    }
}
