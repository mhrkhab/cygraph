package layouts;

import models.Query;
import services.adapters.QueryBuilder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class QueryLayout extends JFrame {

    public QueryLayout(){
        Container cp = getContentPane();
        setTitle("Define Graph Database Query");
        cp.setLayout(new FlowLayout());
        cp.add(new JLabel("Edit your Graph query, then press the button below to execute ..."));
        JTextField txt = new JTextField(35);
        cp.add(txt);

        ArrayList<String> data = new ArrayList<String>();
        ArrayList<Query> queries= Query.getAllQueries();
        if (queries.size() != 0){
            for (Query query : queries.subList(0,Math.min(queries.size(), 4))){
                data.add(query.query);
            }
        }

        JComboBox list = new JComboBox(data.toArray());
        list.insertItemAt("", 0);
        list.setSelectedIndex(0);
        list.setPreferredSize(new Dimension(440,20));
        list.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox)e.getSource();
                txt.setText((String)cb.getSelectedItem());
            }

        });
        cp.add(list);

        Button query = new Button("Execute Query");
        query.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (txt.getText().isBlank() || txt.getText().isEmpty()){
                    new MistakeQuery();
                }
                else {
                    new QueryBuilder(txt.getText());
                    list.removeAllItems();
                    ArrayList<Query> queryList = Query.getAllQueries();
                    for (Query query : queryList.subList(0,Math.min(queryList.size(), 4))){
                        list.addItem(query.query);
                    }
                    list.insertItemAt("", 0);
                    list.setSelectedIndex(0);
                }
            }
        });
        cp.add(query);

        Button cancel = new Button("Cancel");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        cp.add(cancel);

        Button history = new Button("History");
        history.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new QueryHistoryLayout();
            }
        });
        cp.add(history,BorderLayout.WEST);

        pack();
        setVisible(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(445,150);
        setLocation(1000,0);
    }
}
