import helpers.NodesImporter;
import models.*;
import models.missions.CyberAsset;
import models.missions.MissionInformation;
import models.missions.MissionObjective;
import models.missions.MissionTask;
import models.vulnerabilities.*;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DBTest {

    @BeforeAll
    static void beforeAllTests() {
        System.out.println("test models and operations in mongoDB and nodes in Neo4j graphDB");
        System.out.println("******************************************************");
    }

    @Test
    @Order(1)
    void attackPatternsImportTest() {
        AttackPattern at100 = AttackPattern.findById("100");
        if (at100 == null) {
            at100 = new AttackPattern(
                    "100",
                    "Overflow Buffers",
                    "Buffer Overflow attacks target improper or missing bounds checking on buffer operations، typically triggered by input injected by an adversary. As a consequence، an adversary is able to write past the boundaries of allocated buffer regions in memory، causing a program crash or potentially redirection of execution as per the adversaries' choice.",
                    "High",
                    "Very High",
                    "TAXONOMY NAME:WASC:ENTRY ID:07:ENTRY NAME:Buffer Overflow::::TAXONOMY NAME:OWASP Attacks:ENTRY NAME:Buffer overflow attack::",
                    "Draft"
            );
            at100.save();
            at100 = AttackPattern.findById("100");
        }

        System.out.println("AttackPattern 100 saved successfully :: " + at100._id);

        AttackPattern at224 = AttackPattern.findById("224");
        if (at224 == null) {
            at224 = new AttackPattern(
                    "224",
                    "Fingerprinting",
                    "An adversary compares output from a target system to known indicators that uniquely identify specific details about the target. Most commonly، fingerprinting is done to determine operating system and application versions. Fingerprinting can be done passively as well as actively. Fingerprinting by itself is not usually detrimental to the target. However، the information gathered through fingerprinting often enables an adversary to discover existing weaknesses in the target.",
                    "High",
                    "Very Low",
                    "TAXONOMY NAME:WASC:ENTRY ID:45:ENTRY NAME:Fingerprinting::",
                    "Stable"
            );
            at224.save();
            at224 = AttackPattern.findById("224");
        }

        System.out.println("AttackPattern 224 saved successfully :: " + at224._id);

        AttackPattern at142 = AttackPattern.findById("142");
        if (at142 == null) {
            at142 = new AttackPattern(
                    "142",
                    "DNS Cache Poisoning",
                    "A domain name server translates a domain name (such as www.example.com) into an IP address that Internet hosts use to contact Internet resources. An adversary modifies a public DNS cache to cause certain names to resolve to incorrect addresses that the adversary specifies. The result is that client applications that rely upon the targeted cache for domain name resolution will be directed not to the actual address of the specified domain name but to some other address. Adversaries can use this to herd clients to sites that install malware on the victim's computer or to masquerade as part of a Pharming attack.",
                    "High",
                    "High",
                    "TAXONOMY NAME:OWASP Attacks:ENTRY NAME:Cache Poisoning::",
                    "Detailed"
            );
            at142.save();
            at142 = AttackPattern.findById("142");
        }

        System.out.println("AttackPattern 142 saved successfully :: " + at142._id);

        AttackPattern at89 = AttackPattern.findById("89");
        if (at89 == null) {
            at89 = new AttackPattern(
                    "89",
                    "Pharming",
                    "A pharming attack occurs when the victim is fooled into entering sensitive data into supposedly trusted locations، such as an online bank site or a trading platform. An attacker can impersonate these supposedly trusted sites and have the victim be directed to their site rather than the originally intended one. Pharming does not require script injection or clicking on malicious links for the attack to succeed.",
                    "High",
                    "Very High",
                    "",
                    "Standard"
            );
            at89.save();
            at89 = AttackPattern.findById("89");
        }

        System.out.println("AttackPattern 89 saved successfully :: " + at89._id);

        AttackPattern at14 = AttackPattern.findById("14");
        if (at14 == null) {
            at14 = new AttackPattern(
                    "14",
                    "Client-side Injection-induced Buffer Overflow",
                    "This type of attack exploits a buffer overflow vulnerability in targeted client software through injection of malicious content from a custom-built hostile service. This hostile service is created to deliver the correct content to the client software. For example، if the client-side application is a browser، the service will host a webpage that the browser loads.",
                    "Medium",
                    "Very High",
                    "",
                    "Detailed"
            );
            at14.save();
            at14 = AttackPattern.findById("14");
        }

        System.out.println("AttackPattern 14 saved successfully :: " + at14._id);

        AttackPattern at205 = AttackPattern.findById("205");
        if (at205 == null) {
            at205 = new AttackPattern(
                    "205",
                    "key material embedded in client distributions",
                    "This attack pattern has been deprecated as it is a duplicate of CAPEC-37 : Retrieve Embedded Sensitive Data. Please refer to this other pattern going forward.",
                    "High",
                    "High",
                    "",
                    "stable"
            );
            at205.save();
            at205 = AttackPattern.findById("205");
        }

        System.out.println("AttackPattern 205 saved successfully :: " + at205._id);

        AttackPattern at600 = AttackPattern.findById("600");
        if (at600 == null) {
            at600 = new AttackPattern(
                    "600",
                    "Credential Stuffing",
                    "An adversary tries known username/password combinations against different systems، applications، or services to gain additional authenticated access. Credential Stuffing attacks rely upon the fact that many users leverage the same username/password combination for multiple systems، applications، and services. Attacks of this kind often target management services over commonly used ports such as SSH، FTP، Telnet، LDAP، Kerberos، MySQL، and more. Additional targets include Single Sign-On (SSO) or cloud-based applications/services that utilize federated authentication protocols، and externally facing applications. The primary goal of Credential Stuffing is to achieve lateral movement and gain authenticated access to additional systems، applications، and/or services. A successfully executed Credential Stuffing attack could result in the adversary impersonating the victim or executing any action that the victim is authorized to perform. If the password obtained by the adversary is used for multiple systems، accounts، and/or services، this attack will be successful (in the absence of other mitigations). Although not technically a brute force attack، Credential Stuffing attacks can function as such if an adversary possess multiple known passwords for the same user account. This may occur in the event where an adversary obtains user credentials from multiple sources or if the adversary obtains a user's password history for an account. Credential Stuffing attacks are similar to Password Spraying attacks (CAPEC-565) regarding their targets and their overall goals. However، Password Spraying attacks do not have any insight into known username/password combinations and instead leverage common or expected passwords. This also means that Password Spraying attacks must avoid inducing account lockouts، which is generally not a worry of Credential Stuffing attacks. Password Spraying attacks may additionally lead to Credential Stuffing attacks، once a successful username/password combination is discovered.",
                    "High",
                    "High",
                    "TAXONOMY NAME:ATTACK:ENTRY ID:1110.004:ENTRY NAME:Brute Force:Credential Stuffing::::TAXONOMY NAME:OWASP Attacks:ENTRY NAME:Credential stuffing::",
                    "Standard"
            );
            at600.save();
            at600 = AttackPattern.findById("600");
        }

        System.out.println("AttackPattern 600 saved successfully :: " + at600._id);

        AttackPattern at170 = AttackPattern.findById("170");
        if (at170 == null) {
            at170 = new AttackPattern(
                    "170",
                    "Web Application Fingerprinting",
                    "An attacker sends a series of probes to a web application in order to elicit version-dependent and type-dependent behavior that assists in identifying the target. An attacker could learn information such as software versions، error pages، and response headers، variations in implementations of the HTTP protocol، directory structures، and other similar information about the targeted service. This information can then be used by an attacker to formulate a targeted attack plan. While web application fingerprinting is not intended to be damaging (although certain activities، such as network scans، can sometimes cause disruptions to vulnerable applications inadvertently) it may often pave the way for more damaging attacks.",
                    "High",
                    "High",
                    "",
                    "Detailed"
            );
            at170.save();
            at170 = AttackPattern.findById("170");
        }

        System.out.println("AttackPattern 170 saved successfully :: " + at170._id);

        AttackPattern at110 = AttackPattern.findById("110");
        if (at110 == null) {
            at110 = new AttackPattern(
                    "110",
                    "SQL Injection through SOAP Parameter Tampering",
                    "An attacker modifies the parameters of the SOAP message that is sent from the service consumer to the service provider to initiate a SQL injection attack. On the service provider side، the SOAP message is parsed and parameters are not properly validated before being used to access a database in a way that does not use parameter binding، thus enabling the attacker to control the structure of the executed SQL query. This pattern describes a SQL injection attack with the delivery mechanism being a SOAP message.",
                    "High",
                    "Very High",
                    "",
                    "Detailed"
            );
            at110.save();
            at110 = AttackPattern.findById("110");
        }

        System.out.println("AttackPattern 110 saved successfully :: " + at110._id);
    }

    @Test
    @Order(2)
    void machinesImportTest() {
        Machine machine = Machine.findByTitle("LocalDnsServer");
        if (machine == null) {
            machine = new Machine(
                    "LocalDnsServer",
                    "",
                    "192.168.3.30",
                    "55242"
            );
            machine.save();
            machine = Machine.findByTitle("LocalDnsServer");
        }

        System.out.println("Machine saved successfully :: " + machine._id + " :: " + machine.title);

        machine = Machine.findByTitle("Attacker");
        if (machine == null) {
            machine = new Machine(
                    "Attacker",
                    "",
                    "192.168.5.30",
                    "4562"
            );
            machine.save();
            machine = Machine.findByTitle("Attacker");
        }

        System.out.println("Machine saved successfully :: " + machine._id + " :: " + machine.title);

        machine = Machine.findByTitle("malicous.com");
        if (machine == null) {
            machine = new Machine(
                    "malicous.com",
                    "",
                    "390.333.5.23",
                    "4343"
            );
            machine.save();
            machine = Machine.findByTitle("malicous.com");
        }

        System.out.println("Machine saved successfully :: " + machine._id + " :: " + machine.title);

        machine = Machine.findByTitle("MissionClient");
        if (machine == null) {
            machine = new Machine(
                    "MissionClient",
                    "",
                    "42.168.52.30",
                    "9999"
            );
            machine.save();
            machine = Machine.findByTitle("MissionClient");
        }

        System.out.println("Machine saved successfully :: " + machine._id + " :: " + machine.title);

        machine = Machine.findByTitle("DatabaseFrontend");
        if (machine == null) {
            machine = new Machine(
                    "DatabaseFrontend",
                    "",
                    "23.43.5.65",
                    "3433"
            );
            machine.save();
            machine = Machine.findByTitle("DatabaseFrontend");
        }

        System.out.println("Machine saved successfully :: " + machine._id + " :: " + machine.title);

        machine = Machine.findByTitle("DatabaseBackend");
        if (machine == null) {
            machine = new Machine(
                    "DatabaseBackend",
                    "",
                    "324.54.5.75",
                    "5488"
            );
            machine.save();
            machine = Machine.findByTitle("DatabaseBackend");
        }

        System.out.println("Machine saved successfully :: " + machine._id + " :: " + machine.title);
    }

    @Test
    @Order(3)
    void missionTasksImportTest() {
        MissionTask missionTask = MissionTask.findByTitle("mission_task_1");
        if (missionTask == null){
            missionTask = new MissionTask(
                    "mission_task_1",
                    "Cyber Asset",
                    "high",
                    "high"
            );
            missionTask.save();
            missionTask = MissionTask.findByTitle("mission_task_1");
        }

        System.out.println("mission task saved successfully :: " + missionTask._id + " :: " + missionTask.title);

        missionTask = MissionTask.findByTitle("mission_task_2");
        if (missionTask == null){
            missionTask = new MissionTask(
                    "mission_task_2",
                    "mission task 2 description",
                    "high",
                    "high"
            );
            missionTask.save();
            missionTask = MissionTask.findByTitle("mission_task_2");
        }

        System.out.println("mission task saved successfully :: " + missionTask._id + " :: " + missionTask.title);
    }

    @Test
    @Order(4)
    void missionInformationImportTest(){
        MissionInformation missionInformation = MissionInformation.findByTitle("mission_information_1");
        if (missionInformation == null){
            missionInformation = new MissionInformation(
                    "mission_information_1",
                    "mission info description",
                    "high",
                    "high"
            );
            missionInformation.save();
            missionInformation = MissionInformation.findByTitle("mission_information_1");
        }

        System.out.println("mission information saved successfully :: " + missionInformation._id + " :: " + missionInformation.title);

        missionInformation = MissionInformation.findByTitle("mission_information_2");
        if (missionInformation == null){
            missionInformation = new MissionInformation(
                    "mission_information_2",
                    "mission info description",
                    "high",
                    "low"
            );
            missionInformation.save();
            missionInformation = MissionInformation.findByTitle("mission_information_2");
        }

        System.out.println("mission information saved successfully :: " + missionInformation._id + " :: " + missionInformation.title);
    }

    @Test
    @Order(5)
    void cyberAssetsImportTest(){
        CyberAsset cyberAsset = CyberAsset.findByTitle("cyberAsset_1");
        if (cyberAsset == null){
            cyberAsset = new CyberAsset(
                    "cyberAsset_1",
                    "cyberAsset_1 description",
                    "high",
                    "low"
            );
            cyberAsset.save();
            cyberAsset = CyberAsset.findByTitle("cyberAsset_1");
        }

        System.out.println("CyberAsset saved successfully :: " + cyberAsset._id + " :: " + cyberAsset.title);
    }

    @Test
    @Order(6)
    void MissionObjectivesImportTest(){
        MissionObjective missionObjective = MissionObjective.findByTitle("missionObjective_1");
        if (missionObjective == null){
            missionObjective = new MissionObjective(
                    "missionObjective_1",
                    "missionObjective_1 description",
                    "high",
                    "low"
            );
            missionObjective.save();
            missionObjective = MissionObjective.findByTitle("missionObjective_1");
        }

        System.out.println("MissionObjective saved successfully :: " + missionObjective._id + " :: " + missionObjective.title);
    }

    @Test
    @Order(7)
    void subnetsImportTest(){
        Subnet subnet = Subnet.findByTitle("Internet");
        if (subnet == null){
            subnet = new Subnet("Internet");
            subnet.save();
            subnet = Subnet.findByTitle("Internet");
        }

        System.out.println("subnet saved successfully :: " + subnet._id + " :: " + subnet.title);

        subnet = Subnet.findByTitle("DMZ");
        if (subnet == null){
            subnet = new Subnet("DMZ");
            subnet.save();
            subnet = Subnet.findByTitle("DMZ");
        }

        System.out.println("subnet saved successfully :: " + subnet._id + " :: " + subnet.title);

        subnet = Subnet.findByTitle("MissionClients");
        if (subnet == null){
            subnet = new Subnet("MissionClients");
            subnet.save();
            subnet = Subnet.findByTitle("MissionClients");
        }

        System.out.println("subnet saved successfully :: " + subnet._id + " :: " + subnet.title);

        subnet = Subnet.findByTitle("DataCenter");
        if (subnet == null){
            subnet = new Subnet("DataCenter");
            subnet.save();
            subnet = Subnet.findByTitle("DataCenter");
        }

        System.out.println("subnet saved successfully :: " + subnet._id + " :: " + subnet.title);
    }

    @Test
    @Order(8)
    void firewallsImportTest(){
        Firewall firewall = Firewall.findByTitle("internalFirewall");
        if (firewall == null) {
            firewall = new Firewall(
                    "internalFirewall",
                    "internalFirewall"
            );
            firewall.save();
            firewall = Firewall.findByTitle("internalFirewall");
        }

        System.out.println("firewall saved successfully :: " + firewall._id + " :: " + firewall.title);

        firewall = Firewall.findByTitle("externalFirewall");
        if (firewall == null) {
            firewall = new Firewall(
                    "externalFirewall",
                    "externalFirewall"
            );
            firewall.save();
            firewall = Firewall.findByTitle("externalFirewall");
        }

        System.out.println("firewall saved successfully :: " + firewall._id + " :: " + firewall.title);
    }

    @Test
    @Order(9)
    void cveImportTest(){
        CVE cve = CVE.findById("CVE-2004-1754");
        if (cve == null){
            cve = new CVE(
                    "CVE-2004-1754",
                    "CVE-2004-1754",
                    "The DNS proxy (DNSd) for multiple Symantec Gateway Security products allows remote attackers to poison the DNS cache via a malicious DNS server query response that contains authoritative or additional records."
            );
            cve.save();
            cve = CVE.findById("CVE-2004-1754");
        }

        System.out.println("CVE saved successfully :: " + cve._id + " :: " + cve.title);

        cve = CVE.findById("CVE-2021-41322");
        if (cve == null){
            cve = new CVE(
                    "CVE-2021-41322",
                    "CVE-2021-41322",
                    "Poly VVX 400/410 5.3.1 allows low-privileged users to change the Admin password by modifying a POST parameter to 120 during the password reset process."
            );
            cve.save();
            cve = CVE.findById("CVE-2021-41322");
        }

        System.out.println("CVE saved successfully :: " + cve._id + " :: " + cve.title);
    }

    @Test
    @Order(10)
    void snortsImportTest(){
        Snort snort = Snort.findById("1576");
        if (snort == null){
            snort = new Snort("1576");
            snort.save();
            snort = Snort.findById("1576");
        }

        System.out.println("snort saved successfully :: " + snort._id + " :: " + snort.title);

        snort = Snort.findById("33022");
        if (snort == null){
            snort = new Snort("33022");
            snort.save();
            snort = Snort.findById("33022");
        }

        System.out.println("snort saved successfully :: " + snort._id + " :: " + snort.title);
    }

    @Test
    @Order(11)
    void cpeImportTest(){
        CVE cve = CVE.findById("CVE-2004-1754");
        CPE cpe = CPE.findCveCpeByCpe23UriAndCveId("cpe:2.3:a:sap:netweaver_abap:730:*:*:*:*:*:*:*", cve._id);
        if (cpe == null) {
            cpe = new CPE(
                    "CPE-1",
                    "cpe:2.3:a:sap:netweaver_abap:730:*:*:*:*:*:*:*",
                    true,
                    cve._id
            );
            cpe.save();
            cpe = CPE.findCveCpeByCpe23UriAndCveId("cpe:2.3:a:sap:netweaver_abap:730:*:*:*:*:*:*:*", cve._id);
        }

        System.out.println("cpe saved successfully :: " + cpe._id + " :: " + cpe.cpe23Uri);
    }

    @Test
    @Order(12)
    void cweImportTest(){
        CVE cve = CVE.findById("CVE-2004-1754");
        CWE cwe = CWE.findCveCweByCweIdAndCveId("CWE-668", cve._id);
        if (cwe == null) {
            cwe = new CWE("CWE-668", "", "", cve._id);
            cwe.save();
            cwe = CWE.findCveCweByCweIdAndCveId("CWE-668", cve._id);
        }

        System.out.println("CWE saved successfully :: " + cwe._id + " :: " + cwe.id);
    }

    @Test
    @Order(13)
    void cvssImportTest(){
        CVE cve = CVE.findById("CVE-2004-1754");
        CVSS cvss = CVSS.findCveCvssByTitleAndCveId("cvssV3", cve._id);
        if (cvss == null) {
            cvss = new CVSS(
                    "cvssV3",
                    "3.1",
                    "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N",
                    "LOW",
                    "NONE",
                    "NONE",
                    4.3,
                    cve._id
            );
            cvss.save();
            cvss = CVSS.findCveCvssByTitleAndCveId("cvssV3", cve._id);
        }

        System.out.println("CVSS saved successfully :: " + cvss._id + " :: " + cvss.vectorString);
    }

    @Test
    @Order(14)
    void metricsImportTest(){
        CVE cve = CVE.findById("CVE-2004-1754");
        Metric metric = Metric.findCvssMetricByTitleAndCveId("baseMetricV3", cve._id);
        if (metric == null) {
            metric = new Metric(
                    "baseMetricV3",
                    8,
                    2.9,
                    cve._id
            );
            metric.save();
            metric = Metric.findCvssMetricByTitleAndCveId("baseMetricV3", cve._id);
        }

        System.out.println("Metric saved successfully :: " + metric._id + " :: " + metric.title);
    }

    @Test
    @Order(15)
    void severitiesImportTest(){
        CVE cve = CVE.findById("CVE-2004-1754");
        Severity severity = Severity.findByTitleAndCveId("cvssV2", cve._id);
        if (severity == null) {
            severity = new Severity(
                    "cvssV2",
                    "MEDIUM",
                    cve._id
            );
            severity.save();
            severity = Severity.findByTitleAndCveId("cvssV2", cve._id);
        }

        System.out.println("Severity saved successfully :: " + severity._id + " :: " + severity.title);
    }

    @AfterEach
    void afterEachTest() {
        System.out.println("******************************************************");
    }

    @AfterAll
    static void afterAllTest() {
        System.out.println("test of adding nodes in Nao4j and construction of graph topology");
        (new NodesImporter()).run();
        System.out.println("******************************************************");
    }
}