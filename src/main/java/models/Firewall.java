package models;

import org.bson.Document;
import org.bson.types.ObjectId;
import services.db.DBConnection;

import static com.mongodb.client.model.Filters.eq;

public class Firewall extends Node{

    public final static String COLLECTION = "firewalls";

    public Firewall(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Firewall(String title, String description, ObjectId _id) {
        this.title = title;
        this.description = description;
        this._id = _id;
    }

    @Override
    public void save() {
        DBConnection dbConnection = getConnection(COLLECTION);
        Document document = new Document("title",this.title)
                .append("description",this.description);
        saveOrUpdate(dbConnection,document);
    }

    protected static Firewall instance(Document document){
        return document != null ? new Firewall(
                (String)document.get("title"),
                (String)document.get("description"),
                (ObjectId)document.get("_id")
        ) : null;
    }

    public static Firewall findByTitle(String title){
        return instance(
                (new DBConnection(COLLECTION)).find(
                        eq("title",title)
                )
        );
    }

    public static Firewall findByMainId(ObjectId _id) {
        Document document = findByMainIdAndDbCon(
                new DBConnection(COLLECTION),
                _id
        );

        if (document == null){
            return null;
        }
        return instance(document);
    }

    public String getCollection(){
        return COLLECTION;
    }
}
